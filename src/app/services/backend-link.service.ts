import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpRequest, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
// import { Model } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class BackendLinkService {

  anyVar: any = null;
  observedItem = new BehaviorSubject<any>(this.anyVar);
  editDel = new BehaviorSubject<number>(-5);
  added: any = null;

  constructor(private http: HttpClient) { }

  startList(): Observable<any> {
    return this.observedItem.asObservable();
  }
  startEditDel(): Observable<number>{
    return this.editDel.asObservable();
  }

  getList() {
    this.http.get(`${environment.url}/fruits`)
    .toPromise().then((backEnd) => {
      this.anyVar = JSON.parse(JSON.stringify(backEnd));
      this.observedItem.next(this.anyVar);
    });
  }

  setID(x: number){
    this.editDel.next(x);
  }

  clearID(){
    this.editDel.next(-4);
  }

  addFruit(name: string, desc: string, loc: string, shape: string, color: string, formy: FormData ){

    while (name.indexOf(' ') > 0){
      name = name.replace(' ', '%20');
    }
    while (desc.indexOf(' ') > 0){
      desc = desc.replace(' ', '%20');
    }
    while (loc.indexOf(' ') > 0){
      loc = loc.replace(' ', '%20');
    }
    while (shape.indexOf(' ') > 0){
      shape = shape.replace(' ', '%20');
    }
    while (color.indexOf(' ') > 0){
      color = color.replace(' ', '%20');
    }
    this.http.post(`${environment.url}/fruits/add-fruit?name=${name}&description=${desc}&location=${loc}&shape=${shape}&color=${color}`,formy,
    {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).subscribe(
      data => {
        this.added = data;
      },
      (bad: HttpErrorResponse) => {
        console.log("error: " + bad.error);
        console.log("name: " + bad.name);
        console.log("message: " + bad.message);
        console.log("status: " + bad.status);
      }
    );
  }

  deleteFruit(id){

    this.http.delete(`${environment.url}/fruits/${id}/remove`).subscribe();

    this.editDel.next(-3);
  }

  editFruit(name: string, desc: string, loc: string, shape: string, color: string, formy: FormData, id: number){

    while (name.indexOf(' ') > 0){
      name = name.replace(' ', '%20');
    }
    while (desc.indexOf(' ') > 0){
      desc = desc.replace(' ', '%20');
    }
    while (loc.indexOf(' ') > 0){
      loc = loc.replace(' ', '%20');
    }
    while (shape.indexOf(' ') > 0){
      shape = shape.replace(' ', '%20');
    }
    while (color.indexOf(' ') > 0){
      color = color.replace(' ', '%20');
    }
    this.http.post(`${environment.url}/fruits/${id}/edit?name=${name}&description=${desc}&location=${loc}&shape=${shape}&color=${color}`,formy,
    {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).subscribe(
      data => {
        this.added = data;
      },
      (bad: HttpErrorResponse) => {
        console.log("error: " + bad.error);
        console.log("name: " + bad.name);
        console.log("message: " + bad.message);
        console.log("status: " + bad.status);
      }
    );
  }

  test(){
    console.log(this.anyVar);
    console.log(this.observedItem);
  }
  
}
