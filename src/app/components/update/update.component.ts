import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BackendLinkService } from 'src/app/services/backend-link.service';
import { textSpanIntersectsWithTextSpan } from 'typescript';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  observing: any = null;
  editID: number;
  id: number;

  oldName: string;
  oldDesc: string;
  oldLoc: string;
  oldCol: string;
  oldSha: string;

  fg2: FormGroup;

  constructor(private srv: BackendLinkService, private router: Router) { }

  ngOnInit(): void {
    this.srv.startList().subscribe(stuff => this.observing = stuff);
    this.srv.getList();
    this.srv.startEditDel().subscribe(numbers => this.editID = numbers);
    this.id = this.observing[this.editID].id;
    console.log(this.observing);
    console.log(this.id);
    console.log(this.editID);

    this.oldName = this.observing[this.editID].name;
    this.oldDesc = this.observing[this.editID].description;
    this.oldLoc = this.observing[this.editID].location;
    this.oldSha = this.observing[this.editID].shape;
    this.oldCol = this.observing[this.editID].color;

    this.fg2 = new FormGroup({
      name: new FormControl(this.oldName),
      description: new FormControl(this.oldDesc),
      location: new FormControl(this.oldLoc),
      color: new FormControl(this.oldCol),
      shape: new FormControl(this.oldSha),
    })

  }

  edit(){
    let formy = new FormData();
    formy.append('name', this.fg2.get('name').value);
    formy.append('description', this.fg2.get('description').value);
    formy.append('location', this.fg2.get('location').value);
    formy.append('color', this.fg2.get('color').value);
    formy.append('shape', this.fg2.get('shape').value);

    this.srv.editFruit(this.fg2.get('name').value,
    this.fg2.get('description').value,
    this.fg2.get('location').value,
    this.fg2.get('shape').value,
    this.fg2.get('color').value,
    formy,
    this.id);

    this.fg2.reset();
    this.srv.getList();
    this.router.navigate(['/list_fruits']);
  }

}