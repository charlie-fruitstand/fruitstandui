import { Component, OnInit } from '@angular/core';
import { BackendLinkService } from 'src/app/services/backend-link.service';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {
  observing: any = null;
  selectedID: number = null;

  constructor(private srv: BackendLinkService) { }

  ngOnInit(): void {
    this.srv.startList().subscribe(stuff => this.observing = stuff);
    this.srv.getList();
    this.srv.startEditDel().subscribe(numbers => this.selectedID = numbers);
  }

  toChangeData(n: number){
    this.srv.setID(n);
  }

  test(){
    this.srv.test();
    console.log("click");
  }

  test2(x){
    console.log(x);
  }

}
