import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BackendLinkService } from 'src/app/services/backend-link.service';

@Component({
  selector: 'app-destory',
  templateUrl: './destroy.component.html',
  styleUrls: ['./destroy.component.css']
})
export class DestoryComponent implements OnInit {
  observing: any = null;
  deleteID: number;
  id: number;


  constructor(private srv: BackendLinkService, private router: Router) { }

  ngOnInit(): void {
    this.srv.startList().subscribe(stuff => this.observing = stuff);
    this.srv.getList();
    this.srv.startEditDel().subscribe(numbers => this.deleteID = numbers);
    this.id = this.observing[this.deleteID].id;
  }

  confirm(id){
    this.srv.deleteFruit(id);
  }


 

}
