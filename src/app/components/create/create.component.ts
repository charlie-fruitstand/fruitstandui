import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BackendLinkService } from 'src/app/services/backend-link.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  observing: any = null;

  fg = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    location: new FormControl(''),
    color: new FormControl(''),
    shape: new FormControl(''),
  })

  constructor(private srv: BackendLinkService, private router: Router) { }

  ngOnInit(): void {
    this.srv.startList().subscribe(stuff => this.observing = stuff);
    this.srv.getList();
  }

  submit(){

    let formy = new FormData();
    formy.append('name', this.fg.get('name').value);
    formy.append('description', this.fg.get('description').value);
    formy.append('location', this.fg.get('location').value);
    formy.append('color', this.fg.get('color').value);
    formy.append('shape', this.fg.get('shape').value);

    this.srv.addFruit(this.fg.get('name').value,
    this.fg.get('description').value,
    this.fg.get('location').value,
    this.fg.get('shape').value,
    this.fg.get('color').value,
    formy);

    console.log(this.fg.get('name').value);
    console.log(this.fg.get('description').value);
    console.log(this.fg.get('location').value);
    console.log(this.fg.get('color').value);
    console.log(this.fg.get('shape').value);


    this.fg.reset();
    this.router.navigate(['/list_fruits']);
  }

}
