import { NumberLiteralType } from 'typescript';

export interface Model{
    id: number,
    name: string,
    description: string,
    location: string,
    color: string,
    shape: string
}
