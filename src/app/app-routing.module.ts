import { createComponent } from '@angular/compiler/src/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './components/create/create.component';
import { DestoryComponent } from './components/destroy/destroy.component';
import { IndexComponent } from './components/index/index.component';
import { ReadComponent } from './components/read/read.component';
import { UpdateComponent } from './components/update/update.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
const routes: Routes = [
  {
    path:"",
    component: HomeComponent
  },
  {
    path:"add_fruit",
    component: CreateComponent
  },
  {
    path:"list_fruits",
    component: ReadComponent
  },
  {
    path:"edit_fruit",
    component: UpdateComponent
  },
  {
    path:"delete_fruit",
    component: DestoryComponent
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }